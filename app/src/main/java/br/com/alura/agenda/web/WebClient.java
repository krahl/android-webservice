package br.com.alura.agenda.web;

import android.support.annotation.Nullable;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by renan on 20/01/16.
 */
public class WebClient {
    public String post(String json) {
        String endedeco = "https://www.caelum.com.br/mobile";
        return realizaConexao(json, endedeco);
    }
    public void insere(String json) {
        String endedeco = "http://192.168.0.105:8080/api/aluno";
        realizaConexao(json, endedeco);
    }
    @Nullable
    private String realizaConexao(String json, String endedeco) {
        try {
            URL url = new URL(endedeco);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            connection.setDoOutput(true);

            PrintStream output = new PrintStream(connection.getOutputStream());
            output.println(json);

            connection.connect();

            Scanner scanner = new Scanner(connection.getInputStream());
            String resposta = scanner.next();
            return resposta;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
